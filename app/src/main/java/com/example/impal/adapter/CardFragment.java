package com.example.impal.adapter;

import android.os.Bundle;

import android.support.annotation.Nullable;

import android.support.v4.app.Fragment;

import android.support.v7.widget.CardView;

import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;

import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.impal.R;
import com.example.impal.cinemakuy.MovieNowPlayingData;


public class CardFragment extends Fragment {


    public MovieNowPlayingData.DataMovie movie;
    protected CardView cardView;
    protected View view;

    @Nullable
    @Override
    public View getView() {
        return view;
    }

    public void setListener(View.OnClickListener clickListener){
        view.setOnClickListener(clickListener);
    }

    public static Fragment getInstance(int position, MovieNowPlayingData.DataMovie movie) {

        CardFragment f = new CardFragment();
        f.movie = movie;

        Bundle args = new Bundle();

        args.putInt("position", position);

        f.setArguments(args);


        return f;

    }



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,

                             @Nullable Bundle savedInstanceState) {

        View view = setLayout(inflater,container,R.layout.movie_slider , R.id.card_view);

        // Put CodeHere
        ReFresh();



        Log.d("ONCREATEx" , "CREATE 1");
        return view;


        //return null;

    }

    public View setLayout(LayoutInflater inflater,ViewGroup container, int layout,int cardview){
        view = inflater.inflate(layout, container, false);

        cardView = (CardView) view.findViewById(cardview);
        cardView.setMaxCardElevation(cardView.getCardElevation() * CardAdapter.MAX_ELEVATION_FACTOR);

        return view;
    }

    public void ReFresh(){
        if(movie==null){
            return;
        }
        //TextView tv = view.findViewById(R.id.tittle_homepage);
        //tv.setText(movie.name);
        ImageView imageView = view.findViewById(R.id.cover_homepage);
        imageView.setImageBitmap(movie.bitmap);
    }

    public CardView getCardView() {

        return cardView;

    }



}
