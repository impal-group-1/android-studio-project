package com.example.impal.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;

import android.support.v4.app.FragmentStatePagerAdapter;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;

import android.view.View;
import android.view.ViewGroup;


import com.example.impal.HomepageActivity;
import com.example.impal.R;
import com.example.impal.cinemakuy.MovieNowPlayingData;

import java.util.ArrayList;

import java.util.List;



public class CardFragmentPagerAdapter extends FragmentStatePagerAdapter implements CardAdapter {



    private List<CardFragment> fragments;

    private float baseElevation;

    HomepageActivity activity;



    public CardFragmentPagerAdapter(FragmentManager fm, float baseElevation , List<CardFragment> data , HomepageActivity homepageActivity) {

        super(fm);

        fragments = data;

        this.baseElevation = baseElevation;

        activity = homepageActivity;


    }



    @Override

    public float getBaseElevation() {

        return baseElevation;

    }



    @Override

    public CardView getCardViewAt(int position) {

        return fragments.get(position).getCardView();

    }



    @Override

    public int getCount() {

        return fragments.size();

    }



    @Override

    public Fragment getItem(final int position) {

        CardFragment cardFragment = (CardFragment) CardFragment.getInstance(position,fragments.get(position).movie);

        cardFragment.setListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomepageActivity) activity).ToDetailFragment(position);
            }
        });


        return cardFragment;

    }
    public CardFragment getCardFragment(int pos){
        return fragments.get(pos);
    }



    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Object fragment = super.instantiateItem(container, position);

        fragments.set(position, (CardFragment) fragment);



        //fragments.get(position).ReFresh();
        return fragment;

    }



    public void addCardFragment(CardFragment fragment) {

        fragments.add(fragment);

    }

    public void NotifyDataUpdate(){
        //notifyDataSetChanged();

        for (CardFragment cf: fragments) {
            cf.ReFresh();
        }


    }
    public void NotifyDataUpdate(int idx){
        //notifyDataSetChanged();

        fragments.get(idx).ReFresh();


    }
    public int IsMovieExist(MovieNowPlayingData.DataMovie movie){
        for (int i=0;i<fragments.size();i++) {
            if(fragments.get(i).movie.name == movie.name){
                return i;
            }
        }
        return -1;
    }

    public static float dpToPixels(int dp, Context context) {

        return dp * (context.getResources().getDisplayMetrics().density);

    }

    public static CardFragmentPagerAdapter SetUpViewPager (AppCompatActivity activity , int ViewPageID, ArrayList<CardFragment> data , HomepageActivity homepageActivity){
        ViewPager viewPager = (ViewPager) activity.findViewById(ViewPageID);

        CardFragmentPagerAdapter pagerAdapter = new CardFragmentPagerAdapter(activity.getSupportFragmentManager(), CardFragmentPagerAdapter.dpToPixels(2, activity), data , homepageActivity );

        ShadowTransformer fragmentCardShadowTransformer = new ShadowTransformer(viewPager, pagerAdapter);
        fragmentCardShadowTransformer.enableScaling(true);

        viewPager.setAdapter(pagerAdapter);
        viewPager.setPageTransformer(false, fragmentCardShadowTransformer);
        viewPager.setOffscreenPageLimit(3);

        return pagerAdapter;

    }



}
