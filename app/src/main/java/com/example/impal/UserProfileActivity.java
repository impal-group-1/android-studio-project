package com.example.impal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.impal.cinemakuy.CinemakuyAPI;
import com.example.impal.cinemakuy.CinemakuyAPIStatic;
import com.example.impal.cinemakuy.GetAccResponse;
import com.example.impal.cinemakuy.RequestGetAcc;
import com.example.impal.Loading.ViewDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfileActivity extends AppCompatActivity {

    ViewDialog viewDialog;
    CinemakuyAPI cinemakuyAPI = CinemakuyAPIStatic.create();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
    }

    public void profileView(){

        viewDialog.showDialog();
        final String username = PreferencesAccount.PreferencesGet(UserProfileActivity.this).getString("USERNAME","");
        String token = PreferencesAccount.PreferencesGet(UserProfileActivity.this).getString("TOKEN","");

            Call<GetAccResponse> getAccResponseCall = cinemakuyAPI.getAcc(new RequestGetAcc(username,token));

            getAccResponseCall.enqueue(new Callback<GetAccResponse>() {
                @Override
                public void onResponse(Call<GetAccResponse> call, Response<GetAccResponse> response) {

                    GetAccResponse getAccResponse = response.body();
                    if(getAccResponse.isSuccess!=null && getAccResponse.isSuccess){


                    }else{
                        Toast.makeText(UserProfileActivity.this, username , Toast.LENGTH_SHORT).show();
                    }
                    viewDialog.hideDialog();

                }

                @Override
                public void onFailure(Call<GetAccResponse> call, Throwable t) {
                    viewDialog.hideDialog();
                }
            });

    }

    public void loadProfile(){

    }
}
