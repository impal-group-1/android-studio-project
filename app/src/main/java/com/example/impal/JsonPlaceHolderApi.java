package com.example.impal;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceHolderApi {
    @GET("typicode/demo/profile")
    Call<ObjectTest> getObj();
}
