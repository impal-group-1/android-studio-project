package com.example.impal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.impal.cinemakuy.CinemakuyAPI;
import com.example.impal.cinemakuy.CinemakuyAPIStatic;
import com.example.impal.cinemakuy.LoginResponse;
import com.example.impal.cinemakuy.RequestRegister;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RegisterActivity extends AppCompatActivity {

    public class ValidateResponse{
        public Boolean isSuccess = true;
        public String message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.regis_activity);
    }

    public void Register(View view) {
        EditText nameET = (EditText)findViewById(R.id.name_regis);
        EditText userET = (EditText)findViewById(R.id.username_regis);
        EditText emailET = (EditText)findViewById(R.id.email_regis);
        EditText passET = (EditText)findViewById(R.id.pass_regis);
        EditText cpassET = (EditText)findViewById(R.id.cpass_regis);
        EditText phoneET = (EditText)findViewById(R.id.phone_regis);
        //TextView tview = (TextView)findViewById(R.id.textview1);
        String username = userET.getText().toString();
        String password = passET.getText().toString();
        String name = nameET.getText().toString();
        String email = emailET.getText().toString();
        String phone = phoneET.getText().toString();
        String cpassword = cpassET.getText().toString();

        ValidateResponse VR = FormValidator(username,password,cpassword,name,email,phone);
        if(VR.isSuccess){
            RegisterProcess(username,password,name,email,phone);
        }else{
            Toast.makeText(this, "Status : "+VR.message, Toast.LENGTH_SHORT).show();
        }
        //LoginProcess(username , password);


    }


    // ini perlu dibenarkan lagi
    ValidateResponse FormValidator(String username,String pass,String cpass,String name , String email , String phone){
        return new ValidateResponse();
    }

    public void RegisterProcess(String username,String pass,String name , String email , String phone){

        // create cinemakuy API
        CinemakuyAPI cinemakuyAPI = CinemakuyAPIStatic.create();
        // call account/register
        Call<LoginResponse> obj = cinemakuyAPI.register(new RequestRegister( username,pass, name , email, phone));
        // waiting for API response
        obj.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                // Moving movieNowPlayingData data to variable
                LoginResponse result = response.body();

                Toast.makeText(RegisterActivity.this, result.respon, Toast.LENGTH_SHORT).show();
                // is Succesfully registered?
                if(result.isSuccess){
                    // do smoething here case : success

                    Intent myIntent = new Intent( RegisterActivity.this , LoginActivity.class);
                    RegisterActivity.this.startActivity(myIntent);
                }else{
                    // do something here case : failed

                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // Register Failed because no internet connection

            }
        });




    }

    public void Login(View view) {
        // Move to login activity
        Intent myIntent = new Intent(RegisterActivity.this, LoginActivity.class);
        RegisterActivity.this.startActivity(myIntent);
    }
}
