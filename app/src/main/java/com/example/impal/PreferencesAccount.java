package com.example.impal;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.prefs.Preferences;

public class PreferencesAccount {

    // Values for Shared Prefrences

    public static void PreferencesAdd(String key , String value , Activity activity){
        SharedPreferences sharedPreferences = activity.getSharedPreferences("account" , Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();


    }

    public static SharedPreferences PreferencesGet(Activity activity){
        return activity.getSharedPreferences("account" , Context.MODE_PRIVATE);
    }

}
