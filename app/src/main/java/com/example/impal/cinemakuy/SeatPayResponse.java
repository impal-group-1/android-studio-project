package com.example.impal.cinemakuy;

public class SeatPayResponse {
    public class SeatPayResponseData{
        public int order_id;
        public int purchase_status;
        public String share_message;
        public String payment_type;
        public class  PaymentChannel{
            public class VirtualAccount{
                public String bank;
                public String va_number;
            }
            public class GoPay{
                public String qr_code;
                public String deeplink_url;
            }
            public VirtualAccount virtual_account = new VirtualAccount();
            public GoPay gopay = new GoPay();
        }
        public PaymentChannel payment_channel = new PaymentChannel();

    }
    public SeatPayResponseData data = new SeatPayResponseData();
    public int status;
    public String message;
}
