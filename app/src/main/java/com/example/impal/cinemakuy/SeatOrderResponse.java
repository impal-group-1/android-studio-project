package com.example.impal.cinemakuy;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SeatOrderResponse {
    public class SeatOrderResponseData{
        public int order_id;
        public String booking_reference;
        public int event_id;
        public String total_price;
        public int timeout_period;
        public class PaymentComponent{
            public String payment_title;
            public String payment_description;
        }
        public ArrayList<PaymentComponent> payment_component = new ArrayList<>();
        public class AllowedPayment{
            public String payment_provider_name;
            public int payment_provider_type;
            public String charge_type;
            public String payment_fee;
            public String payment_total;
        }
        public ArrayList<AllowedPayment> allowed_payment = new ArrayList<>();
        public class Ticket{
            public int ticket_id;
            public String name;
            public int quantity;
            public int price;
            public String seats;
        }
        public ArrayList<Ticket> tickets = new ArrayList<>();
        public String name;
        public String image;
        public String showdate;
        public String showtime;
        public String seats;
        public String cinema_name;
        public String region;
    }
    public SeatOrderResponseData data = new SeatOrderResponseData();
    public int status;
    public String message;

}
