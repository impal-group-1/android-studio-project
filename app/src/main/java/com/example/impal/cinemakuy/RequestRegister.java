package com.example.impal.cinemakuy;

public class RequestRegister {
    String username;
    String pass;
    String name;
    String email;
    String phone;

    public RequestRegister(String username, String pass, String name, String email, String phone) {
        this.username = username;
        this.pass = pass;
        this.name = name;
        this.email = email;
        this.phone = phone;
    }
}
