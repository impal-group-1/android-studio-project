package com.example.impal.cinemakuy;

import java.util.ArrayList;

public class SeatOrderRequest {

    public ArrayList<SeatsData.SeatsSchedule.SeatLayout.Seats> seats = new ArrayList<>();
    public String audi;
    public int event_id;
    public String provider;

}
