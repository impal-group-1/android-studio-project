package com.example.impal.cinemakuy;

import android.graphics.Bitmap;

import java.util.ArrayList;

public class MovieNowPlayingData {

    public class DataMovie{
        public int event_id;
        public String name;
        public String image;
        public Bitmap bitmap;
        public boolean pre_order;
        public String rating;
        public String genre;
        public String director;
        public String actor;
        public String duration;
        public String synopsis;
        public Boolean recommended;
        public String tickets_sold;
        public String IMDb_rating;
        public int parent_id;

    }
    public String messege;
    public int status;
    public ArrayList<DataMovie> data = new ArrayList<>();
}

