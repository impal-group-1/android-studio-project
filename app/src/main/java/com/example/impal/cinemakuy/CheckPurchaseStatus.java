package com.example.impal.cinemakuy;

public class CheckPurchaseStatus {

    public class CheckPurchaseStatusData{
        public int order_id;
        public int purchase_status;
        public String share_message;
        public int fnb_purchase_status;
        public String fnb_share_message;
        private String provider;
    }
    public CheckPurchaseStatusData data = new CheckPurchaseStatusData();
    public int status;
    public String message;
}
