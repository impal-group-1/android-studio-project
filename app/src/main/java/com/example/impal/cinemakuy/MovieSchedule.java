package com.example.impal.cinemakuy;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class MovieSchedule {
    public class MovieData{
        public class ShowDates{
            public String showdate;
            public String day;
            public String date;
            public Boolean available;
        }
        public class Cinemas{
            public String name;
            public String provider_name;
            public int event_id;
            public String provider_image;
            public String address;
            public String city;
            public class SchedulesList{
                public String schedule_class;
                public String showdate;
                public class Schedules{
                    public int schedule_id;
                    public String showtime;
                    public int price;
                    public int seat_available;
                }
                public ArrayList<Schedules> schedules = new ArrayList<>();
            }
            public ArrayList<SchedulesList> schedules_lists = new ArrayList<>();
        }
        public ArrayList<ShowDates> showdates = new ArrayList<>();
        public ArrayList<Cinemas> cinemas = new ArrayList<>();

    }
    public MovieData data = new MovieData();
    public int status;
    public String message;
}
