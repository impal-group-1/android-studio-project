package com.example.impal;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.impal.adapter.ShadowTransformer;
import com.example.impal.sliderfeature.DepthPageTransformer;
import com.example.impal.sliderfeature.ShadowsTransformer;
import com.example.impal.sliderfeature.fragments.CardPage;
import com.example.impal.sliderfeature.fragments.Intro;

public class SliderPagePrototype extends FragmentActivity {

    private static final int NUM_PAGES = 5;

    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider_page_prototype);

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.view_pager_home);
        pagerAdapter = new SliderPagePrototype.ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(pagerAdapter);

        mPager.setPageTransformer(true, new ShadowsTransformer());
    }


    String[] data = {"asdasd","123123123","fhsdcnlajndjbd","2819ajsao","0813 fbanlsjoa"};
    public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            CardPage intro = new CardPage();
            intro.setTittle(data[position]);
            return intro;
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }
}
