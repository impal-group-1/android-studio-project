package com.example.impal;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TestAPI extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_api);
    }
    private void testAPI(final TextView tv){
        /// sample
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://my-json-server.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonPlaceHolderApi jsonPlaceHolderApi = retrofit.create(JsonPlaceHolderApi.class);

        Call<ObjectTest> obj = jsonPlaceHolderApi.getObj();

        obj.enqueue(new Callback<ObjectTest>() {
            @Override
            public void onResponse(Call<ObjectTest> call, Response<ObjectTest> response) {
                ObjectTest objectTest = response.body();
                String result = "Result : "+objectTest.getName();
                Toast.makeText(TestAPI.this,result , Toast.LENGTH_SHORT).show();
                tv.setText(result);
            }

            @Override
            public void onFailure(Call<ObjectTest> call, Throwable t) {

            }
        });
    }

    public void test(View view) {
        TextView tv = findViewById(R.id.sampleText);
        Toast.makeText(TestAPI.this, "API is working please wait" , Toast.LENGTH_SHORT).show();
        testAPI(tv);
    }
}
