package com.example.impal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.impal.adapter.AdapterMovieHomepage;
import com.example.impal.adapter.CardFragment;
import com.example.impal.adapter.CardFragmentPagerAdapter;
import com.example.impal.cinemakuy.CinemakuyAPI;
import com.example.impal.cinemakuy.CinemakuyAPIStatic;
import com.example.impal.cinemakuy.MovieNowPlayingData;
import com.google.gson.Gson;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomepageActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView information;
    CinemakuyAPI cinemakuyAPI;
    AdapterMovieHomepage adapter;
    SharedPreferences prefs;
    ArrayList<String> citylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_homepage);
        //Define object in layout
        progressBar = findViewById(R.id.p_bar);
        information = findViewById(R.id.information_homepage);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);


        prefs = this.getSharedPreferences(
                "movie", Context.MODE_PRIVATE);

        //create a Cinemakuy API Holder
        cinemakuyAPI = CinemakuyAPIStatic.create();

        //create dropdown-list
        SetSpinner();

        //Generate recycler view
        adapter = new AdapterMovieHomepage(new MovieNowPlayingData() , HomepageActivity.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(HomepageActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);




    }

    public void ToDetailFragment(int position) {
        ToDetail(cardHomeCardFragmentPagerAdapter.getCardFragment(position).movie);
    }

    public void ToDetail(int i){/*
        Intent intent = new Intent(HomepageActivity.this , DetailActivity.class);
        //https://developer.android.com/reference/android/content/Intent.html#FLAG_ACTIVITY_CLEAR_TOP
        Gson gson = new Gson();

        intent.putExtra("cover", movieNowPlayingData.data.get(i).bitmap );
        .bitmap = null;
        String jsonFilm = gson.toJson(movieNowPlayingData.data.get(i));
        intent.putExtra("dataFilm" ,jsonFilm );

        startActivity(intent);*/
        ToDetail(movieNowPlayingData.data.get(i));
    }

    public void ToDetail(MovieNowPlayingData.DataMovie data){
        Intent intent = new Intent(HomepageActivity.this , DetailActivity.class);
        //https://developer.android.com/reference/android/content/Intent.html#FLAG_ACTIVITY_CLEAR_TOP
        Gson gson = new Gson();

        intent.putExtra("cover", data.bitmap );
        data.bitmap = null;
        String jsonFilm = gson.toJson(data);
        intent.putExtra("dataFilm" ,jsonFilm );

        startActivity(intent);
    }


    CardFragmentPagerAdapter cardHomeCardFragmentPagerAdapter;
    void GenerateHiglight(){

        ArrayList<CardFragment> cardFragments = new ArrayList<>();

        for(int i=0; i<movieNowPlayingData.data.size(); i++){
            if(movieNowPlayingData.data.get(i).recommended) {
                CardFragment cf = new CardFragment();
                cf.movie = movieNowPlayingData.data.get(i);
                Log.d("CARD",cf.movie.name);
                cardFragments.add(cf);
            }
        }

        cardHomeCardFragmentPagerAdapter = CardFragmentPagerAdapter.SetUpViewPager(
                this ,R.id.viewPager, cardFragments,HomepageActivity.this
        );
    }



    void SetSpinner(){
        // Turn on Progress bar and Hide Text Info
        progressBar.setVisibility(View.VISIBLE);
        information.setVisibility(View.GONE);

        //Call /movies/cities
        Call<ArrayList<String>> cities = cinemakuyAPI.getCity();
        //Waiting for Response
        final Spinner spinner = findViewById(R.id.cities_homepage);
        //setSpinner to default
        ArrayList<String> list = new ArrayList<String>();
        list.add(prefs.getString("city" , "Jakarta"));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);


        cities.enqueue(new Callback<ArrayList<String>>() {
            @Override
            public void onResponse(Call<ArrayList<String>> call, Response<ArrayList<String>> response) {
                // Turn off waiting bar
                progressBar.setVisibility(View.GONE);
                // Moving Data to variable 'citylist'
                citylist = response.body();
                // Make and Set an array adapter for spinner
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(HomepageActivity.this,android.R.layout.simple_spinner_item , citylist);
                arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(arrayAdapter);

                // Get default selection
                int idx = citylist.indexOf(prefs.getString("city" , "Jakarta"));
                spinner.setSelection( ( idx==-1 ? 0 : idx ) );

                // Add listener for spinner
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView parent, View view, int position, long id) {
                        // Get current city
                        String currentCity = parent.getItemAtPosition(position).toString();
                        // Detect is current city is equal than before?
                        if(prefs.getString("city" , "Jakarta") != currentCity){
                            // Saving data to phone localdata
                            Boolean res = prefs.edit().putString("city" , currentCity).commit();
                            // Clear Recyclerview current data
                            ClearRecyclerView();
                            // Regenerate Recycler view
                            GenerateRecyclerView(currentCity);
                        }

                    }
                    @Override
                    public void onNothingSelected(AdapterView parent) {
                        // if there's no item selected then back to default select
                        int idx = citylist.indexOf(prefs.getString("city" , "Jakarta"));
                        parent.setSelection( ( idx==-1 ? 0 : idx ) );
                    }
                });

            }

            @Override
            public void onFailure(Call<ArrayList<String>> call, Throwable t) {
                // No Intenet connection handling
                progressBar.setVisibility(View.GONE);
                information.setText("No internet connection");
                information.setVisibility(View.VISIBLE);
            }
        });
    }

    // Clearing data on recycler view
    void ClearRecyclerView(){
        adapter.clear();
    }

    MovieNowPlayingData movieNowPlayingData;
    Call<MovieNowPlayingData> movieNowPlayingDataCall;

    // Generating new recycler view
    void GenerateRecyclerView(String city){
        // Turn on Loading bar
        progressBar.setVisibility(View.VISIBLE);
        information.setVisibility(View.GONE);

        // if there's exist API that still working then turn it off
        if(movieNowPlayingDataCall != null) {movieNowPlayingDataCall.cancel();}

        // Call movies/now_playing
        movieNowPlayingDataCall = cinemakuyAPI.getNowPlaying(city);
        // Waiting for Response
        movieNowPlayingDataCall.enqueue(new Callback<MovieNowPlayingData>() {
            @Override
            public void onResponse(Call<MovieNowPlayingData> call, Response<MovieNowPlayingData> response) {
                // Moving data to object
                movieNowPlayingData = response.body();
                Log.d("MOVIE", movieNowPlayingData.data.get(0).name);
                // Turn progress bar off
                progressBar.setVisibility(View.GONE);
                // is we get the data? 200 means completely successfully
                if(movieNowPlayingData.status==200) {
                    //Asynchronus load image bitmap
                    UpdateBitMap updateBitMap = new UpdateBitMap();
                    updateBitMap.setMovieNowPlayingData(movieNowPlayingData);
                    updateBitMap.execute("");
                    // Renew Recycler view, trigger change data in adapter
                    adapter.generate(movieNowPlayingData);
                    GenerateHiglight();
                }else{
                    // Give information to user that something error in back-end
                    information.setText("Cant get message plase contact administrator");
                    information.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<MovieNowPlayingData> call, Throwable t) {
                // No internet connection handler, if there's no internet
                Log.d("MOVIE", "Failed: "+t.getMessage());
                progressBar.setVisibility(View.GONE);
                information.setText("No internet connection");
                information.setVisibility(View.VISIBLE);
            }
        });
    }

    // Update when bitmap is updated
    public void UpdateAdapter(int i){
        Log.d("REFRES", "index update: "+i);
        // Trigger adapter that item at 'i' is changed
        adapter.notifyItemChanged(i);
    }
    // Update when bitmap is updated
    public void UpdateSlider(int i){
        Log.d("REFRES", "index update: "+i);
        // Trigger adapter that item at 'i' is changed
        cardHomeCardFragmentPagerAdapter.NotifyDataUpdate(i);
    }



    // Asyncrhonus method
    private class UpdateBitMap extends AsyncTask<String, Void, String> {
        private MovieNowPlayingData movieNowPlayingData;
        public void setMovieNowPlayingData(MovieNowPlayingData movieNowPlayingData){
            this.movieNowPlayingData = movieNowPlayingData;
        }
        @Override
        protected String doInBackground(String... params) {
            // Get bitmap from URL
            for(int i=0;i<movieNowPlayingData.data.size(); i++) {
                try {
                    URL url = null;
                    url = new URL(movieNowPlayingData.data.get(i).image);
                    Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                    movieNowPlayingData.data.get(i).bitmap = bmp;
                    int idxSlider = -1;
                    if(movieNowPlayingData.data.get(i).recommended) {
                        idxSlider = cardHomeCardFragmentPagerAdapter.IsMovieExist(movieNowPlayingData.data.get(i));
                    }


                    final int idx2 = idxSlider;
                    final int idx= i;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            // Run on UI Thread that bitmap at 'idx' is succesfuly getted
                            UpdateAdapter(idx);
                            if(idx2 != -1) {
                                UpdateSlider(idx2);
                            }
                        }
                    });
                }catch (IOException e){
                    Log.e("Async method", "doInBackground: "+e.getMessage() );
                }
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {

        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

}
