package com.example.impal.sliderfeature;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;

import com.example.impal.SliderPagePrototype;

public class ShadowsTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(@NonNull View view, float position) {
        int pageWidth = view.getWidth();
        Log.d("POSITION" , "Pos:"+position);
        float pos = (pageWidth/10)*(position/2);
        Log.d("POSITION2" , "PosViews:"+pos);
        view.setTranslationX(pos);
        view.setScaleX((1-Math.abs(position)) / 3);
        view.setScaleY((1-Math.abs(position)) / 1.3f);



    }
}
